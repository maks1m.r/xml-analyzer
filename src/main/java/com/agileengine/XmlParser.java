package com.agileengine;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.util.Optional;

public class XmlParser {

    private static final Logger LOGGER = LoggerFactory.getLogger(XmlParser.class);

    private static final String CHARSET_NAME = "utf8";

    public Optional<Element> findElementById(String htmlFilePath, String targetElementId) {

        return findElementById(new File(htmlFilePath), targetElementId);

    }

    public Optional<Element> findElementById(File htmlFile, String targetElementId) {
        try {
            Document doc = Jsoup.parse(
                    htmlFile,
                    CHARSET_NAME,
                    htmlFile.getAbsolutePath());

            return Optional.ofNullable(doc.getElementById(targetElementId));

        } catch (IOException e) {
            LOGGER.error("Error reading file: {}", htmlFile.getAbsolutePath(), e);
            return Optional.empty();
        }
    }

    public Optional<Elements> findElementsByQuery(String htmlFilePath, String cssQuery) {

        return findElementsByQuery(new File(htmlFilePath), cssQuery);

    }

    public Optional<Elements> findElementsByQuery(File htmlFile, String cssQuery) {
        try {
            Document doc = Jsoup.parse(
                    htmlFile,
                    CHARSET_NAME,
                    htmlFile.getAbsolutePath());

            return Optional.ofNullable(doc.select(cssQuery));

        } catch (IOException e) {
            LOGGER.error("Error reading file: {}", htmlFile.getAbsolutePath(), e);
            return Optional.empty();
        }
    }
}
