package com.agileengine;

import lombok.Builder;
import lombok.Getter;

import java.util.List;

@Builder
@Getter
public class ElementMatch {

    private ElementHolder elementHolder;

    private Integer matchScore;

    private List<String> matchAnalysis;

}
